/**
 *   Copyright (C) 2012-2019 Oslandia <infos@oslandia.com>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

#include "graph_builder.hh"

#include <boost/format.hpp>
#include <iostream>
#include <fstream>
#include <memory>

#include "db.hh"
#include "graph.hh"

namespace Tempus
{

using namespace std;

void import_constants( Db::Connection& connection, Graph& graph )
{
    // transport modes
    RoutingData::TransportModes modes = load_transport_modes( connection );
    graph.set_transport_modes( modes );
}

// mapping between road section ID and road arcs
using RoadSectionMap = std::map<Tempus::db_id_t, Graph::arc_descriptor>;

///
/// Function used to import the graph from a PostgreSQL database.
std::unique_ptr<Graph> import_graph( Db::Connection& connection, const std::string& schema_name )
{
    RoadSectionMap road_sections_map;
    
    // locally maps db ID to Node or Section
    std::map<Tempus::db_id_t, Graph::node_descriptor> road_nodes_map;
    //------------------
    //   Nodes
    //------------------
    std::vector<Tempus::Node> nodes;
    // count nodes to reserve memory
    {
        Db::Result res = connection.exec( (boost::format("SELECT COUNT(*) FROM %1%.node") % schema_name).str() );
        size_t count = 0;
        res[0][0] >> count;
        nodes.reserve( count );
    }
    {
        Db::ResultIterator res_it = connection.exec_it( (boost::format("SELECT id, "
                                                                       "st_x(geom), "
                                                                       "st_y(geom), "
                                                                       "st_z(geom) "
                                                                       "FROM tempus_networks.node") % schema_name).str() );
        Db::ResultIterator it_end;
        for ( ; res_it != it_end; res_it++ )
        {
            Db::RowValue res_i = *res_it;
            Tempus::Node node;

            node.set_db_id( res_i[0] );

            Point3D p;
            p.set_x( res_i[1] );
            p.set_y( res_i[2] );
            p.set_z( res_i[3] );
            node.set_coordinates(p);

            road_nodes_map[node.db_id()] = nodes.size();
            nodes.push_back( node );
        }
    }

    //------------------
    //   Arcs
    //------------------
    Graph::ArcMap arcs;
    {
        //
        // Get an arc and its opposite, if present
        const std::string qquery = (boost::format("SELECT "
                                                  "id, "
                                                  "node_from_id, "
                                                  "node_to_id, "
                                                  "is_pt, "
                                                  "traffic_rules "
                                                  "FROM tempus_networks.arc"
                                                  ) % schema_name).str();
        Db::ResultIterator res_it = connection.exec_it( qquery );
        Db::ResultIterator it_end;
        for ( ; res_it != it_end; res_it++ )
        {
            Db::RowValue res_i = *res_it;
            Arc arc;
            int j = 0;

            arc.set_db_id( res_i[j++].as<db_id_t>() );

            db_id_t node_from_id = res_i[j++].as<db_id_t>();
            db_id_t node_to_id = res_i[j++].as<db_id_t>();
            TEMPUS_ASSERT( node_from_id > 0 );
            TEMPUS_ASSERT( node_to_id > 0 );

            bool is_pt = res_i[j++].as<bool>();
            arc.set_type( is_pt ? Arc::PublicTransportArc : Arc::RoadArc );

            int32_t traffic_rules = res_i[j++].as<int>();
            arc.set_traffic_rules( traffic_rules );

            // Assert that corresponding nodes exist
            TEMPUS_ASSERT_MSG( road_nodes_map.find( node_from_id ) != road_nodes_map.end(),
                              ( boost::format( "Non existing node_from %1% on road_section %2%" ) % node_from_id % arc.db_id() ).str().c_str() );
            TEMPUS_ASSERT_MSG( road_nodes_map.find( node_to_id ) != road_nodes_map.end(),
                              ( boost::format( "Non existing node_to %1% on road_section %2%" ) % node_to_id % arc.db_id() ).str().c_str() );
            Graph::node_descriptor v_from = road_nodes_map[ node_from_id ];
            Graph::node_descriptor v_to = road_nodes_map[ node_to_id ];

            arcs[std::make_pair(v_from, v_to)] = arc;
        }

        
    }

    std::unique_ptr<Graph> graph( new Graph( arcs, nodes ) );
#if 0
    //-----------------------
    //   Turn restrictions
    //-----------------------
    {
        Road::GraphProperties& gp = get_property( *road_graph );
        gp.restrictions() = import_turn_restrictions( *road_graph, connection, schema_name );
    }
#endif
    //---------------------------------
    //           Constants
    //---------------------------------
    import_constants( connection, *graph );

    //---------------------------------
    //   Public transport timetables
    //---------------------------------
    return graph;
}

std::unique_ptr<RoutingData> GraphBuilder::pg_import( const std::string& pg_options, const VariantMap& options ) const
{
    Db::Connection connection( pg_options );

    std::string schema_name = "tempus_core_import";
    auto schema_it = options.find( "schema_name" );
    if ( schema_it != options.end() ) {
        schema_name = schema_it->second.str();
    }

    std::unique_ptr<Graph> graph( import_graph( connection, schema_name ) );
    std::unique_ptr<RoutingData> rgraph( graph.release() );
    return rgraph;
}

std::unique_ptr<Graph> GraphBuilder::build_from_iterators( RecordIterator<TransportMode>& tm_iterator,
                                                           RecordIterator<Node>& node_iterator,
                                                           RecordIterator<ArcRecord>& arc_iterator )
{
    
    //load_transport_modes( 

    // locally maps db ID to Node or Section
    std::map<Tempus::db_id_t, Graph::node_descriptor> road_nodes_map;
    //------------------
    //   Nodes
    //------------------
    std::vector<Tempus::Node> nodes;
    nodes.reserve( node_iterator.count() );

    do {
        Optional<Node> node = node_iterator.value();
        if ( node ) {
            road_nodes_map[node->db_id()] = nodes.size();
            nodes.push_back( *node );
        }
    } while ( node_iterator.next() );

    //------------------
    //   Arcs
    //------------------
    Graph::ArcMap arcs;
    do {
        Optional<ArcRecord> arc = arc_iterator.value();
        if ( arc ) {
            TEMPUS_ASSERT( arc->node_from_id > 0 );
            TEMPUS_ASSERT( arc->node_to_id > 0 );

            // Assert that corresponding nodes exist
            TEMPUS_ASSERT_MSG( road_nodes_map.find( arc->node_from_id ) != road_nodes_map.end(),
                              ( boost::format( "Non existing node_from %1% on road_section %2%" ) % arc->node_from_id % arc->arc.db_id() ).str().c_str() );
            TEMPUS_ASSERT_MSG( road_nodes_map.find( arc->node_to_id ) != road_nodes_map.end(),
                              ( boost::format( "Non existing node_to %1% on road_section %2%" ) % arc->node_to_id % arc->arc.db_id() ).str().c_str() );
            Graph::node_descriptor v_from = road_nodes_map[ arc->node_from_id ];
            Graph::node_descriptor v_to = road_nodes_map[ arc->node_to_id ];

            arcs[std::make_pair(v_from, v_to)] = arc->arc;
        }
    } while ( arc_iterator.next() );

    std::unique_ptr<Graph> graph( new Graph( arcs, nodes ) );

    //---------------------------------
    //           Constants
    //---------------------------------
    graph->set_transport_modes( load_transport_modes( tm_iterator ) );

    return graph;
}

}
