/**
 *   Copyright (C) 2019-2020 Oslandia <infos@oslandia.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "multimodal_routing.hh"

#include <boost/heap/d_ary_heap.hpp>
#include <boost/optional/optional_io.hpp>
#include <tuple>

#include "datetime.hh"
#include "restriction.hh"


namespace Tempus {

    ///
    /// A multimodal node descriptor with a graph node and a mode id
    struct NodeMode {
        Graph::node_descriptor node;
        db_id_t transport_mode_id;

        bool operator==( const NodeMode& other ) const {
            return std::tie( node, transport_mode_id ) == std::tie(other.node, other.transport_mode_id );
        }
        bool operator!=( const NodeMode& other ) const {
            return std::tie( node, transport_mode_id ) != std::tie(other.node, other.transport_mode_id );
        }
        bool operator<( const NodeMode& other ) const {
            return std::tie( node, transport_mode_id ) < std::tie( other.node, other.transport_mode_id );
        }
    }; 

    ///
    /// A multimodal node augmented with a restriction automaton state 
    struct NodeModePTState {
        Graph::node_descriptor node;
        db_id_t transport_mode_id;
        bool pt;
        ArcRestrictionAutomaton::State automaton_state = 0;

        bool operator==( const NodeModePTState& other ) const {
            return std::tie(node, transport_mode_id, pt, automaton_state) == std::tie(other.node, other.transport_mode_id, other.pt, other.automaton_state);
        }
        bool operator!=( const NodeModePTState& other ) const {
            return std::tie(node, transport_mode_id, pt, automaton_state) != std::tie(other.node, other.transport_mode_id, other.pt, other.automaton_state);
        }
        bool operator<( const NodeModePTState& other ) const {
            return std::tie(node, transport_mode_id, pt, automaton_state) < std::tie(other.node, other.transport_mode_id, other.pt, other.automaton_state);
        }
    };
    
    //
    // Data structure used inside the Dijkstra-like algorithm
    struct ObjData
    {
        ///
        /// Internal id, assigned after request has been solved
        int64_t id;
        
        ///
        /// Generalized cost cumulated from the origin, in specific unit
        float potential = std::numeric_limits<float>::max();
        
        /// 
        /// Predecessor object in the paths
        NodeModePTState predecessor;
        
        ///
        /// Current (incoming or outcoming depending on the time constraint type) arc
        Optional<Graph::arc_descriptor> arc;
        
        ///
        /// current time from the origin of the graph, in seconds
        int32_t total_time = 0;
        
        ///
        /// Fare cumulated from the origin
        float total_fare = 0;

        /// current public transport trip id
        Optional<db_id_t> pt_trip_id;     
        
        ///
        /// Current wait time to consider before the next public transport depart, in seconds
        int32_t waiting_time;
        
        /// Current departure time, in seconds
        int32_t departure_time = 0;
        int32_t arrival_time = 0;
        
        /// 
        /// Walking time cumulated from the origin, in seconds
        int32_t total_walking_time = 0; 
        
        ///
        /// Bicycle time cumulated from the origin, in seconds
        int32_t total_bicycle_time = 0;
        
        ///
        /// Number of mode changes cumulated from the origin
        int16_t total_mode_changes = 0;

        /// 
        /// Number of public transport changes cumulated from the origin
        int16_t total_pt_transfers = -1;
        
        /// 
        /// Tells if the object belongs to requested paths (define after algorithm execution, always false when max_cost is defined)
        bool in_paths = false;
    };
    
    struct ArcData
    {
        ///
        /// Internal id, assigned after request has been solved
        int64_t id;
        
        ///
        /// Transport mode ID
        db_id_t transport_mode_id;
        
        ///
        /// Generalized cost cumulated from the origin, in specific unit
        float potential = std::numeric_limits<float>::max();
        
        /// 
        /// Predecessor object in the paths
        NodeModePTState predecessor;
        
        ///
        /// current time from the origin of the graph, in seconds
        int32_t total_time = 0;
        
        ///
        /// Fare cumulated from the origin
        float total_fare = 0;

        /// current public transport trip id
        Optional<db_id_t> pt_trip_id;     
                
        ///
        /// Current wait time to consider before the next public transport depart, in seconds
        int32_t waiting_time;
        
        /// Current departure time, in seconds
        int32_t departure_time = 0;
        int32_t arrival_time = 0;
        
        /// 
        /// Walking time cumulated from the origin, in seconds
        int32_t total_walking_time = 0; 
        
        ///
        /// Bicycle time cumulated from the origin, in seconds
        int32_t total_bicycle_time = 0;
        
        ///
        /// Number of mode changes cumulated from the origin
        int16_t total_mode_changes = 0;

        /// 
        /// Number of public transport changes cumulated from the origin
        int16_t total_pt_transfers = -1;
        
        /// 
        /// Tells if the object belongs to requested paths (define after algorithm execution, always false when max_cost is defined)
        bool in_paths = false;
    };

    typedef std::map<NodeModePTState, ObjData> ObjDataMap;
    //typedef std::map<ArcModePTState, ObjData> TempObjDataMap;
    typedef std::map<Graph::arc_descriptor, ArcData> ArcDataMap;

    struct NullHeuristic
    {
        float operator() ( const Graph::node_descriptor& ) const
        {
            return 0.0;
        }
    };

    #if 0
    //
    // Comparison operator that may include an A* like heuristic
    template <class Object, class ObjDataMap, class Heuristic >
    struct HeuristicCompare
    {
        const ObjDataMap& pmap_;
        Heuristic h_;
        HeuristicCompare( const ObjDataMap& pmap, Heuristic h ) : pmap_(pmap), h_(h) {}
        bool operator()( const Object& a, const Object& b ) const {
            return (pmap_.find(a)->second.potential + h_(a.node)) > (pmap_.find(b)->second.potential + h_(b.node));
        }
    };
    #endif

    //
    // Convert a time offset to seconds
    static int32_t duration_to_seconds( const TimeOffset& to )
    {
        return static_cast<int32_t>( to.total_milliseconds() / 1000 );
    }

    template <bool is_direction_forward>
    Graph::node_descriptor arc_ending( const Graph::arc_descriptor& e, const Graph& graph )
    {
        if ( is_direction_forward ) {
            return target( e, graph );
        }
        return source( e, graph );
    }

    /**
     * Implementation of the Dijkstra algorithm (label-setting) for a multimodal graph
     *
     * \param graph
     * \param starting_time The time at the beginning of the query.
     * \param source
     * \param targets Set of targets that need to be met before returning
     * \param maximum_cost Maximum cost to reach before returning. If set, targets are ignored.
     * \param data_map
     * \param allowed_modes
     * \returns the set of reached target (node, mode, state) triplets (empty for isochrone)
     */
    template <bool is_direction_forward>
    std::set<NodeModePTState> multimodal_dijkstra( const Graph& graph,
                                                 Optional<DateTime> starting_time,
                                                 const NodeMode source,
                                                 std::set<NodeMode> targets,
                                                 Optional<float> maximum_cost,
                                                 OptimizationConstraints oc,
                                                 GeneralizedCostWeights w,
                                                 ObjDataMap& data_map,
                                                 ArcDataMap& arc_data_map,
                                                 const std::vector<TransportMode>& allowed_modes )
    {   
        std::set<NodeModePTState> reached_targets;
        const ArcRestrictionAutomaton& automaton = is_direction_forward ? graph.restriction_automaton() : graph.reverse_restriction_automaton();

        //typedef HeuristicCompare<NodeModePTState, ObjDataMap, std::function<double (const Graph::node_descriptor&)> > Cmp;
        //Cmp cmp( data_map, heuristic );

        // FIXME: we use an indirection here for sorting the priorty queue
        // we could perhaps directly store the potential in the queue
        std::function<bool (const NodeModePTState&, const NodeModePTState&)> cmp;
        cmp = [&]( const NodeModePTState& a, const NodeModePTState& b) {
            return data_map.find(a)->second.potential > data_map.find(b)->second.potential;
        }; 
        
        typedef boost::heap::d_ary_heap< NodeModePTState, boost::heap::arity<4>, boost::heap::compare< decltype(cmp) >, boost::heap::mutable_<true> > NodeQueue;
        NodeQueue node_queue( cmp );
        
        // Default potential
        float default_cost = std::numeric_limits<float>::max();
        int32_t default_time = 0;
        float default_fare = 0.0;
        
        // time offset from the common time origin of the graph
        int32_t starting_time_offset = 0;
        DateTime origin = graph.time_origin() ? *graph.time_origin() : *starting_time;
        if ( starting_time ) {
            starting_time_offset = duration_to_seconds( *starting_time - origin );
            COUT2 << "Time = " << *starting_time << std::endl;
        }
        COUT2 << "Time origin: "<< origin << ", offset = " << starting_time_offset << " seconds" << std::endl;
        
        // FIXME check for negative offset ??
        
        NodeModePTState source_object;
        source_object.node = source.node ;
        source_object.transport_mode_id = source.transport_mode_id ;
        source_object.pt = false;
        source_object.automaton_state = automaton.initial_state_ ;
        node_queue.push( source_object );
        
        ObjData s;
        s.potential = 0.0;
        s.total_time = 0;
        s.total_fare = 0;
        s.predecessor = source_object;
        data_map[source_object] = s;
        
        NodeModePTState min_object;
        while ( !node_queue.empty() ) {            
            // Examine node of the min object 
            min_object = node_queue.top();
            node_queue.pop();
            
            assert( data_map.find( min_object ) != data_map.end() );
            const ObjData min_data = data_map[min_object];
            
            COUT2 << "Examined object = ( node: " << graph[min_object.node].db_id() << ", mode: " << min_object.transport_mode_id << ", PT: " << min_object.pt << ", automaton_state: " << min_object.automaton_state << " )";
            COUT2 << ", potential = " << min_data.potential << ", time = "<< min_data.total_time << ", fare = " << min_data.total_fare ;
            COUT2 << ", mode changes = " << min_data.total_mode_changes << ", PT transfers = " << min_data.total_pt_transfers << ", walking time = " << min_data.total_walking_time << ", bicycle time = "<< min_data.total_bicycle_time ;
            if (min_data.pt_trip_id) COUT2 << ", PT trip ID = " << *(min_data.pt_trip_id);
            COUT2 << std::endl;
            
            // Test the stop condition
            if ( min_data.potential < std::numeric_limits<float>::max() ) 
            {
                if ( maximum_cost ) { // Isochron case
                    if ( min_data.potential >=  maximum_cost.get() ) {
                        COUT2 << "  Break: current potential >= " << maximum_cost.get() << std::endl;
                        break;
                    }
                }
                else { // Shortest path tree case
                    auto target_it = targets.find( NodeMode({min_object.node, min_object.transport_mode_id}) );
                    if ( target_it != targets.end() ) {
                        COUT2 << "  Target found ! " << std::endl;
                        targets.erase( target_it );
                        reached_targets.insert( min_object );
                    }
                    if ( targets.empty() ) {
                        COUT2 << "  Break: all targets found ! " << std::endl;
                        break;
                    }
                }
            }
            // Test transitions to each allowed mode
            for ( const TransportMode& fin_mode : allowed_modes ) 
            {
                NodeModePTState new_object;
                ObjData new_data;
                TransportMode ini_mode = *(graph.transport_mode( min_object.transport_mode_id ) );
                            
                if ( min_object.transport_mode_id == fin_mode.db_id() ) {
                    // No mode change: examine out arcs
                    auto relax_arc = [&]( const Graph::arc_descriptor& current_arc ) {
                        const Arc& arc = graph[current_arc];
                        // If the current mode is not allowed on this arc, skip it
                        if ( !( graph[current_arc].type() == Arc::RoadArc ) || ( graph[current_arc].traffic_rules() & fin_mode.traffic_rule() ) ) {
                            new_object.node = arc_ending<is_direction_forward>(current_arc, graph);
                            new_object.transport_mode_id = fin_mode.db_id();
                            if (graph[current_arc].type() == Arc::RoadArc) new_object.pt = false;
                            else new_object.pt = true;
                            
                            GeneralizedCost c = is_direction_forward ? Tempus::compute_forward_arc_cost( arc, fin_mode, starting_time_offset+min_data.total_time, min_data.pt_trip_id, graph[min_object.node].pt_boarding_security_time() )
                                                                     : Tempus::compute_backward_arc_cost( arc, fin_mode, starting_time_offset-min_data.total_time, min_data.pt_trip_id, graph[min_object.node].pt_boarding_security_time() ); 
                            
                            float travel_cost = std::numeric_limits<float>::max();                        
                            // Check if the maximum constraints are reached
                            if ( c.time < std::numeric_limits<int32_t>::max() ) {
                                bool allowed = false;
                                if ( arc.type() == Arc::RoadArc ) {
                                    if ( fin_mode.category() == TransportMode::Walking ) {
                                        if ( ( !oc.max_walking_time ) || ( ( oc.max_walking_time ) && ( min_data.total_walking_time + c.time <= *(oc.max_walking_time) ) ) ) {
                                            allowed = true;
                                        }
                                    }
                                    else if ( fin_mode.category() == TransportMode::Bicycle ) {
                                        if ( ( !oc.max_bicycle_time ) || ( ( oc.max_bicycle_time ) && ( min_data.total_bicycle_time + c.time <= *(oc.max_bicycle_time) ) ) ) {
                                            allowed = true;
                                        }
                                    }
                                    else allowed = true ; // fin_mode.category() == TransportMode::Car 
                                    if (allowed) travel_cost = c.time * w.indiv_mode_time_weight + c.fare * w.fare_weight; 
                                } 
                                else if ( arc.type() == Arc::PublicTransportArc ) {
                                    if ( ( c.pt_trip_id ) && ( ( !min_data.pt_trip_id ) || ( c.pt_trip_id != min_data.pt_trip_id ) ) ) {
                                        if ( oc.max_pt_transfers ) {
                                            if ( min_data.total_pt_transfers + 1 > *(oc.max_pt_transfers) ) {
                                                allowed = true;
                                            }
                                        }
                                        else allowed = true; // no constraint on the number of PT transfers
                                    }
                                    else allowed = true; // no PT transfer
                                    if (allowed) {
                                        travel_cost = c.pt_time * w.pt_time_weight + c.waiting_time * w.waiting_time_weight + c.fare * w.fare_weight;
                                    } 
                                }
                            }
                            
                            if ( travel_cost < std::numeric_limits<float>::max() ) { // Examine restriction penalties                            
                                ArcRestrictionAutomaton::State s;
                                bool found;
                                std::tie( s, found ) = automaton.find_transition( min_object.automaton_state, arc.db_id() );
                                if ( found ) new_object.automaton_state = s;
                                else new_object.automaton_state = 0;
                                if ( new_object.automaton_state != min_object.automaton_state ) {
                                    Optional<GeneralizedCost> restriction_penalty = automaton.penalty( new_object.automaton_state, fin_mode );
                                    if ( restriction_penalty ) {
                                        COUT2 << "  Transition to state " << s << ", restriction penalty: time = " << restriction_penalty->time << ", fare = " << restriction_penalty->fare << ", total cost = " << restriction_penalty->time + restriction_penalty->fare * w.fare_weight << std::endl;
                                        if ( restriction_penalty->time < std::numeric_limits<int>::max() ) {
                                            c.time += restriction_penalty->time; 
                                            travel_cost += restriction_penalty->time;
                                        }
                                        else {
                                            c.time = std::numeric_limits<int>::max();
                                            travel_cost = std::numeric_limits<float>::max();
                                        }
                                        if ( restriction_penalty->fare < std::numeric_limits<float>::max() ) {
                                            c.fare += restriction_penalty->fare;
                                            travel_cost += restriction_penalty->fare * w.fare_weight;
                                        }
                                        else {
                                            c.fare = std::numeric_limits<float>::max();
                                            travel_cost = std::numeric_limits<float>::max();
                                        }
                                    }
                                }
                            }
                            
                            bool is_new_object = false;
                            auto n_it = data_map.find( new_object );
                            if ( n_it != data_map.end() ) {
                                new_data = n_it->second;
                            }
                            else {
                                new_data.potential = default_cost;
                                new_data.total_time = default_time;
                                new_data.total_fare = default_fare;
                                is_new_object = true;
                            }
                            COUT2 << "  - Object: ( node: " << graph[new_object.node].db_id() << ", mode: " << new_object.transport_mode_id << ", PT: "<< new_object.pt << ", automaton_state: "<< new_object.automaton_state << " )";
                            COUT2 << ", potential = "<< new_data.potential << ", time = "<< new_data.total_time << ", fare = " << new_data.total_fare ;
                            COUT2 << ", arc ID = "<< arc.db_id() << ", travel cost = " << travel_cost ;
                            if (new_data.pt_trip_id) COUT2 << ", trip ID = "<< *(new_data.pt_trip_id);                                                 
                            
                            if ( (travel_cost < std::numeric_limits<float>::max()) && ( min_data.potential + travel_cost < new_data.potential ) ) {
                                // backup of the old value
                                if ( ( !is_new_object ) && ( new_data.arc ) ) { 
                                    auto it = arc_data_map.find(*(new_data.arc));
                                    if ( (it==arc_data_map.end()) || ((it->second).potential > new_data.potential) ) {
                                        ArcData adata;
                                        adata.transport_mode_id = new_object.transport_mode_id;
                                        adata.predecessor = new_data.predecessor;
                                        adata.potential = new_data.potential;
                                        adata.total_time = new_data.total_time;
                                        adata.total_fare = new_data.total_fare;
                                        if (new_data.pt_trip_id) adata.pt_trip_id = *(new_data.pt_trip_id);
                                        adata.waiting_time = new_data.waiting_time;
                                        adata.departure_time = new_data.departure_time;
                                        adata.arrival_time = new_data.arrival_time;
                                        adata.total_walking_time = new_data.total_walking_time;
                                        adata.total_bicycle_time = new_data.total_bicycle_time;
                                        adata.total_mode_changes = new_data.total_mode_changes;
                                        adata.total_pt_transfers = new_data.total_pt_transfers;
                                        arc_data_map[*(new_data.arc)] = adata;
                                    }
                                }
                                new_data.potential = min_data.potential + travel_cost;
                                new_data.predecessor = min_object;
                                new_data.arc = current_arc;
                                new_data.total_bicycle_time = min_data.total_bicycle_time + c.bicycle_time;              
                                new_data.total_walking_time = min_data.total_walking_time + c.walking_time;
                                new_data.waiting_time = c.waiting_time;
                                if ( c.pt_trip_id ) {
                                    new_data.pt_trip_id = *c.pt_trip_id;
                                    if ( ( !min_data.pt_trip_id ) || ( new_data.pt_trip_id != min_data.pt_trip_id ) ) {
                                        new_data.total_pt_transfers = min_data.total_pt_transfers + 1;
                                    }
                                    else new_data.total_pt_transfers = min_data.total_pt_transfers;                                    
                                }                                    
                                else {
                                    new_data.pt_trip_id = Optional<db_id_t>();                                
                                    new_data.total_pt_transfers = min_data.total_pt_transfers;
                                }
                                new_data.total_time = min_data.total_time + c.time ; 
                                if ( is_direction_forward ) {
                                    new_data.departure_time = starting_time_offset + min_data.total_time + new_data.waiting_time;
                                    new_data.arrival_time = starting_time_offset + new_data.total_time;
                                }
                                else {
                                    new_data.departure_time = starting_time_offset - new_data.total_time;
                                    new_data.arrival_time = starting_time_offset - min_data.total_time - new_data.waiting_time;
                                }
                                new_data.total_fare = min_data.total_fare + c.fare ;
                                new_data.total_mode_changes = min_data.total_mode_changes ;
                                data_map[new_object] = new_data;
                                COUT2 << "  => relaxed: new potential = " << new_data.potential << ", new time = " << new_data.total_time << ", new waiting time = " << new_data.waiting_time << ", new departure time = " << new_data.departure_time << ", new arrival time = " << new_data.arrival_time ;  
                                if (new_data.pt_trip_id) COUT2 << ", new trip ID = " << *(new_data.pt_trip_id);
                                if (is_new_object) node_queue.push( new_object ); 
                            }
                            else if ( ( maximum_cost ) && ( new_data.arc ) ) { // Backup even if it is not relaxed (for isochrons)
                                auto it = arc_data_map.find(current_arc);
                                if ( (it==arc_data_map.end()) || ((it->second).potential > min_data.potential + travel_cost) ) {
                                    ArcData adata;
                                    adata.transport_mode_id = new_object.transport_mode_id;
                                    adata.predecessor = min_object;
                                    adata.potential = min_data.potential + travel_cost;
                                    adata.total_time = min_data.total_time + c.time;
                                    adata.total_fare = min_data.total_fare + c.fare;
                                    if ( c.pt_trip_id ) { 
                                        adata.pt_trip_id = *c.pt_trip_id;
                                        if ( ( !min_data.pt_trip_id ) || ( new_data.pt_trip_id != min_data.pt_trip_id ) ) {
                                            adata.total_pt_transfers = min_data.total_pt_transfers + 1;
                                        }
                                        else adata.total_pt_transfers = min_data.total_pt_transfers; 
                                    }                                        
                                    adata.waiting_time = c.waiting_time;
                                    if ( is_direction_forward ) {
                                        adata.departure_time = starting_time_offset + min_data.total_time + new_data.waiting_time;
                                        adata.arrival_time = starting_time_offset + new_data.total_time;
                                    }
                                    else {
                                        adata.departure_time = starting_time_offset - new_data.total_time;
                                        adata.arrival_time = starting_time_offset - min_data.total_time - new_data.waiting_time;
                                    } 
                                    adata.total_walking_time = min_data.total_walking_time + c.walking_time;
                                    adata.total_bicycle_time = min_data.total_bicycle_time + c.bicycle_time;
                                    adata.total_mode_changes = min_data.total_mode_changes ;
                                    arc_data_map[current_arc] = adata;
                                    COUT2 << " => arc " << graph[current_arc].db_id() << " not relaxed but stored, arc_data_map size = "<< arc_data_map.size(); 
                                }
                            }
                            COUT2 <<std::endl; 
                        }
                    }; // function relax_arc
                    
                    if ( is_direction_forward ) {
                        BGL_FORALL_OUTEDGES_T( min_object.node, out_arc, graph, Graph ) {
                            relax_arc( out_arc );
                        }
                    }
                    else {
                        BGL_FORALL_INEDGES_T( min_object.node, in_arc, graph, Graph ) {
                            relax_arc( in_arc );
                        }
                    }
                }            
                else if ( ( !oc.max_mode_changes ) || ( min_data.total_mode_changes < *(oc.max_mode_changes) ) ) {                      
                    // Check if the maximum number of mode changes is reached
                    // There is a mode change: examine the predecessor node with the new mode
                    GeneralizedCost c = is_direction_forward ? graph.compute_forward_mode_changing_cost( min_object.node, ini_mode, fin_mode ) 
                                                             : graph.compute_backward_mode_changing_cost( min_object.node, ini_mode, fin_mode ); 
                    
                    COUT2 << "  - Object: ( node: " << graph[min_object.node].db_id() << ", mode: " << fin_mode.db_id() << ", automaton_state: "<< new_object.automaton_state << " ), mode changing time = " << c.time << ", cost = " << c.fare << std::endl;
                    
                    float mode_changing_cost = std::numeric_limits<float>::max();  
                    if ( c.time < std::numeric_limits<int32_t>::max() ) {                
                        mode_changing_cost = c.time + w.fare_weight * c.fare; 
                        new_object.node = min_object.node;
                        new_object.transport_mode_id = fin_mode.db_id();
                        new_object.pt = min_object.pt;
                        new_object.automaton_state = min_object.automaton_state;                    
                        
                        bool is_new_object=false;
                        auto n_it = data_map.find( new_object );
                        if ( n_it != data_map.end() ) {
                            new_data = n_it->second;
                        }
                        else {
                            new_data.potential = default_cost;
                            is_new_object = true;
                        }
                        if ( min_data.potential + mode_changing_cost < new_data.potential ) {
                            COUT2 << "    Relaxed: initial potential = " << new_data.potential <<", new potential = "<< min_data.potential + mode_changing_cost << std::endl;
                            new_data.potential = min_data.potential + mode_changing_cost ;
                            new_data.total_time = min_data.total_time + c.time;
                            new_data.total_fare = min_data.total_fare + c.fare;
                            new_data.predecessor = min_object;
                            new_data.arc = Optional<Graph::arc_descriptor>();
                            new_data.waiting_time = 0;
                            if ( is_direction_forward ) { 
                                new_data.departure_time = starting_time_offset + min_data.total_time;
                                new_data.arrival_time = starting_time_offset + new_data.total_time;
                            }
                            else {
                                new_data.departure_time = starting_time_offset - new_data.total_time;
                                new_data.arrival_time = starting_time_offset - min_data.total_time;
                            }
                            new_data.pt_trip_id = Optional<db_id_t>(); 
                            new_data.total_walking_time = min_data.total_walking_time;
                            new_data.total_bicycle_time = min_data.total_bicycle_time;
                            new_data.total_mode_changes = min_data.total_mode_changes+1;
                            new_data.total_pt_transfers = min_data.total_pt_transfers;
                            data_map[new_object] = new_data;
                            if ( is_new_object ) node_queue.push( new_object );
                        }
                    }
                } // else (mode change)
            } // for (transport modes)        
        }
        return reached_targets;
    }


    #define STRING_FORMAT(x) \
        ((std::stringstream&)(std::stringstream() << x)).str()
    
    Tempus::Steps multimodal_routing( const Graph& graph, const Request& request ) 
    {
        auto starting_node = graph.node_from_id( request.starting_node() );
        if ( ! starting_node ) {
            throw std::invalid_argument( STRING_FORMAT( "Unknown source node ID " << request.starting_node() ) );
        }
        
        ObjDataMap data_map; 
        //TempObjDataMap temp_data_map; 
        ArcDataMap arc_data_map;
        NodeMode starting_nodemode; 
        std::set<NodeMode> ending_nodemodes; 
        std::vector<TransportMode> transport_modes; 
        
        starting_nodemode.node = starting_node.get();
        starting_nodemode.transport_mode_id = request.starting_transport_mode();
        
        if ( ! request.maximum_cost() ) {
            for ( size_t i = 0; i < request.ending_nodes().size(); i++ ) {
                NodeMode target;
                db_id_t ending_node_id = request.ending_nodes()[i];
                auto w = graph.node_from_id( ending_node_id );
                if ( ! w ) {
                    throw std::invalid_argument( STRING_FORMAT( "Unknown destination node ID " << ending_node_id ) );
                }
                target.node = w.get(); 
                target.transport_mode_id = request.ending_transport_mode(); 
                ending_nodemodes.insert( target ); 
            }
        }
        
        for ( db_id_t transport_mode_id : request.allowed_transport_modes() ) {
            Optional<TransportMode> tm = graph.transport_mode( transport_mode_id );
            if ( ! tm ) {
                throw std::invalid_argument( STRING_FORMAT( "Unknown transport mode ID " << transport_mode_id ) );
            }
            transport_modes.push_back( *tm );
        }
        
        GeneralizedCostWeights weights; 
        weights.pt_time_weight = request.pt_time_weight();
        weights.waiting_time_weight = request.waiting_time_weight();
        weights.indiv_mode_time_weight = request.indiv_mode_time_weight();
        weights.fare_weight = request.fare_weight();
        weights.pt_transfer_weight = request.pt_transfer_weight();
        weights.mode_change_weight = request.mode_change_weight();
        
        OptimizationConstraints opt_constraints;
        if (request.max_mode_changes()) opt_constraints.max_mode_changes = *(request.max_mode_changes());
        if (request.max_pt_transfers()) opt_constraints.max_pt_transfers = *(request.max_pt_transfers());
        if (request.max_walking_time()) opt_constraints.max_walking_time = *(request.max_walking_time());
        if (request.max_bicycle_time()) opt_constraints.max_bicycle_time = *(request.max_bicycle_time());
        
        std::set<NodeModePTState> reached_targets = request.time_constraint_type() != Request::ConstraintBefore ?
            multimodal_dijkstra<true>( graph,
                                       (request.time_constraint_type() != Request::NoConstraint) ? request.time_constraint() : Optional<DateTime>(),
                                       starting_nodemode,
                                       ending_nodemodes,
                                       request.maximum_cost(),
                                       opt_constraints,
                                       weights, 
                                       data_map,
                                       arc_data_map,
                                       transport_modes )
            :
            multimodal_dijkstra<false>( graph,
                                        request.time_constraint_type() != Request::NoConstraint ? request.time_constraint() : Optional<DateTime>(),
                                        starting_nodemode,
                                        ending_nodemodes,
                                        request.maximum_cost(),
                                        opt_constraints,
                                        weights, 
                                        data_map,
                                        arc_data_map,
                                        transport_modes );
        
        // assign an ID to each step
        db_id_t step_id = 0;
        for ( auto& p : data_map ) {
            p.second.id = ++step_id; 
        }
        for ( auto& p : arc_data_map ) {
            p.second.id = ++step_id;
        }
        
        Tempus::Steps result;
        auto add_step_to_result = [&]( const NodeModePTState& v, const ObjData& d , const bool& in_paths ) {
            Step step;
            step.id = d.id;
            step.node_id = (graph[v.node]).db_id();
            if ( v != d.predecessor ) {
                step.pred_id = data_map[d.predecessor].id;
                step.pred_node_id = (graph[d.predecessor.node]).db_id();
            }
            if ( d.arc ) 
                step.arc_id = graph[ *(d.arc) ].db_id(); 
            step.transport_mode_id = v.transport_mode_id;
            step.automaton_state = v.automaton_state;
            step.cost = d.potential;
            step.pred_cost = data_map[d.predecessor].potential;
            if (( v.pt ) && ( d.pt_trip_id )) step.pt_trip_id = *(d.pt_trip_id);
            step.total_time = d.total_time;
            step.waiting_time = d.waiting_time;
            step.departure_time = d.departure_time;
            step.arrival_time = d.arrival_time;
            step.total_walking_time = d.total_walking_time;
            step.total_bicycle_time = d.total_bicycle_time;
            step.total_fare = d.total_fare;
            step.total_pt_transfers = std::max<int16_t>( d.total_pt_transfers, 0 );
            step.total_mode_changes = d.total_mode_changes;
            if ( request.maximum_cost() ) step.in_paths = in_paths;
            else step.in_paths = d.in_paths;
            COUT2 << "Object = (node: " << step.node_id << ", mode: " << step.transport_mode_id << ", automaton state: " << step.automaton_state << "), arc ID = " << step.arc_id << ", in paths = " << step.in_paths << ", accumulated cost = " << step.cost << ", PT trip ID: " << step.pt_trip_id << std::endl; 
            COUT2 << "  Predecessor = (node: " << step.pred_node_id << ", mode: " << d.predecessor.transport_mode_id << ", automaton state: " << d.predecessor.automaton_state << ")" << std::endl;
            result.push_back( step );
        };
        
        auto add_temp_step_to_result = [&]( const Graph::arc_descriptor& a, const ArcData& d ) {
            Step step;
            step.id = d.id;
            if (source(a, graph) == d.predecessor.node) step.node_id = (graph[target(a, graph)]).db_id();
            else step.node_id = (graph[source(a, graph)]).db_id();
            step.pred_node_id = (graph[d.predecessor.node]).db_id();
            step.arc_id = graph[ a ].db_id(); 
            step.transport_mode_id = d.transport_mode_id; 
            step.cost = d.potential;
            step.pred_cost = data_map[d.predecessor].potential;
            if ( d.pt_trip_id ) step.pt_trip_id = *(d.pt_trip_id);
            step.total_time = d.total_time;
            step.waiting_time = d.waiting_time;
            step.departure_time = d.departure_time;
            step.arrival_time = d.arrival_time;
            step.total_walking_time = d.total_walking_time;
            step.total_bicycle_time = d.total_bicycle_time;
            step.total_fare = d.total_fare;
            step.total_pt_transfers = std::max<int16_t>( d.total_pt_transfers, 0 );
            step.total_mode_changes = d.total_mode_changes;
            step.in_paths = false;
            COUT2 << "Temp object = (arc: " << step.arc_id << ", mode: " << step.transport_mode_id << ", in paths = " << step.in_paths << ", accumulated cost = " << step.cost << ", PT trip ID: " << step.pt_trip_id << std::endl; 
            COUT2 << "  Predecessor = (node: " << step.pred_node_id << ", mode: " << d.predecessor.transport_mode_id << ", automaton state: " << d.predecessor.automaton_state << ")" << std::endl;
            result.push_back( step );
        };
        
        float max_cost = 0.0; 
        std::set<Graph::arc_descriptor> arcs; 
        if ( ! request.maximum_cost() ) {
            for ( const NodeModePTState& target : reached_targets ) {   
                auto v_it = data_map.find( target );
                while ( v_it != data_map.end() ) {
                    ObjData& d = v_it->second;
                    if ( d.potential > max_cost ) max_cost = d.potential;
                    d.in_paths = true;
                    if ( v_it->second.predecessor == v_it->first )
                        break;
                    v_it = data_map.find( d.predecessor );
                }
            }
        }
        else max_cost = *( request.maximum_cost() );

        // Objects belonging to the shortest paths tree
        for ( const auto& p : data_map ) {
            if ( p.second.potential <= max_cost ) {
                 add_step_to_result( p.first, p.second, true );
                 if ( ( request.maximum_cost() ) && ( p.second.arc ) ) arcs.insert( *( p.second.arc ) );
            }
        }
        
        // Objects not belonging to the shortest paths tree
        if ( request.maximum_cost() ) {
            for ( const auto& p : arc_data_map ) {
                if ( p.second.potential <= max_cost ) {
                    auto n_it = arcs.find( p.first );
                    if ( n_it == arcs.end() ) {
                        add_temp_step_to_result( p.first, p.second ); 
                    }
                }
            }
        }
        return result;
    }
} // namespace Tempus

