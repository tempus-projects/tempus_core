/**
 *   Copyright (C) 2012-2014 IFSTTAR (http://www.ifsttar.fr)
 *   Copyright (C) 2012-2014 Oslandia <infos@oslandia.com>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

#include "io.hh"
#include <iostream>

namespace Tempus
{
TEMPUS_EXPORT std::mutex iostream_mutex; // its a plain old global variable

int OutputStream::sync()
{
    const std::string& s = str();
    if ( printf_set ) {
        printf_function( s.c_str(), s.size() );
        str("");
        return 0;
    }
    std::cout.write( s.c_str(), s.size() );
    str("");
    return 0;
}

void OutputStream::set_printf_function( PrintfFunction foo )
{
    printf_set = true;
    printf_function = foo;
}

void OutputStream::unset_printf_function()
{
    printf_set = false;
}


OutputStream tempus_out;
OutputStream tempus_err;
TEMPUS_EXPORT std::ostream tempus_out_stream(&tempus_out);
TEMPUS_EXPORT std::ostream tempus_err_stream(&tempus_err);

void set_out_printf_function( PrintfFunction foo )
{
    tempus_out.set_printf_function( foo );
}
void set_err_printf_function( PrintfFunction foo )
{
    tempus_err.set_printf_function( foo );
}

void unset_out_printf_function()
{
    tempus_out.unset_printf_function();
}
void unset_err_printf_function()
{
    tempus_err.unset_printf_function();
}

}
