#include "routing_data_builder.hh"
#ifdef _WIN32
#   define NOMINMAX
#   include <windows.h>
#endif

namespace Tempus {


RoutingDataBuilderRegistry& RoutingDataBuilderRegistry::instance()
{
    static RoutingDataBuilderRegistry* instance_ = 0;

    if ( 0 == instance_ ) {
        instance_ = new RoutingDataBuilderRegistry();
    }

    return *instance_;
}

void RoutingDataBuilderRegistry::addBuilder( std::unique_ptr<RoutingDataBuilder> a_builder )
{
    builders_[a_builder->name()] = std::move(a_builder);
}

const RoutingDataBuilder* RoutingDataBuilderRegistry::builder( const std::string& name )
{
    auto it = builders_.find( name );
    if ( it != builders_.end() ) {
        return it->second.get();
    }
    return nullptr;
}

std::vector<std::string> RoutingDataBuilderRegistry::builder_list() const
{
    std::vector<std::string> names;
    for ( const auto& p : builders_ ) {
        names.push_back( p.first );
    }
    return names;
}

const char TEMPUS_DUMP_FILE_MAGIC[] = "TDBF";

void RoutingDataBuilder::write_header( std::ostream& ostr ) const
{
    ostr.write( TEMPUS_DUMP_FILE_MAGIC, 4 );
    char nname[256] = {0};
    strncpy( nname, name().c_str(), name().size() );
    ostr.write( nname, 256 );
    uint32_t v = version();
    ostr.write( reinterpret_cast<const char*>( &v ), sizeof( uint32_t ) );
}

void RoutingDataBuilder::read_header( std::istream& istr ) const
{
    char magic[5];
    istr.read( magic, 4 );
    magic[4] = 0;
    if ( std::string( magic ) != std::string( TEMPUS_DUMP_FILE_MAGIC ) ) {
        throw std::runtime_error( std::string( "Unrecognized header " ) + magic );
    }
    char nname[256];
    istr.read( nname, 256 );
    if ( std::string( nname ) != name() ) {
        throw std::runtime_error( "Bad routing data type " + std::string( nname ) + ", expected " + name() );
    }
    uint32_t v;
    istr.read( reinterpret_cast<char*>( &v ), sizeof( uint32_t ) );
    if ( v > version() ) {
        throw std::runtime_error( "Wrong version" );
    }
    std::cout << "Read header of type " << name() << std::endl;
}

std::unique_ptr<RoutingData> RoutingDataBuilder::pg_import( const std::string& /*pg_options*/, const VariantMap& /*options*/ ) const
{
    return std::unique_ptr<RoutingData>();
}
void RoutingDataBuilder::pg_export( const RoutingData* /*rd*/, const std::string& /*pg_options*/, const VariantMap& /*options*/ ) const
{
}
std::unique_ptr<RoutingData> RoutingDataBuilder::file_import( const std::string& /*filename*/, const VariantMap& /*options*/ ) const
{
    return std::unique_ptr<RoutingData>();
}
void RoutingDataBuilder::file_export( const RoutingData* /*rd*/, const std::string& /*filename*/, const VariantMap& /*options*/ ) const
{
}

static const std::map<std::string, TransportMode::Category> transport_mode_mapping = {
        {"Walking", TransportMode::Walking},
        {"Car", TransportMode::Car},
        {"Bicyle", TransportMode::Bicycle}
};

RoutingData::TransportModes load_transport_modes( Db::Connection& connection )
{
    RoutingData::TransportModes modes;

    Db::Result res( connection.exec( "SELECT id, name, category, traffic_rule, speed_rule, toll_rule, vehicle_parking_rule FROM pgtempus.transport_mode" ) );

    for ( size_t i = 0; i < res.size(); i++ ) {
        db_id_t db_id=0;
        res[i][0] >> db_id;
        TEMPUS_ASSERT( db_id > 0 );
        // populate the global variable
        TransportMode t;
        t.set_db_id( db_id );
        t.set_name( res[i][1] );
        std::string category_str = res[i][2];
        auto cat_it = transport_mode_mapping.find( category_str );
        t.set_category( cat_it == transport_mode_mapping.end() ? TransportMode::Walking : cat_it->second );
        t.set_traffic_rule( res[i][3] );
        if ( ! res[i][4].is_null() ) t.set_speed_rule( res[i][4] );
        else t.set_speed_rule( 0 );
        if ( ! res[i][5].is_null() ) t.set_toll_rule( res[i][5] );
        else t.set_toll_rule( 0 );
        if ( ! res[i][6].is_null() ) t.set_vehicle_parking_rule( res[i][6] );
        else t.set_vehicle_parking_rule( 0 );

        modes[t.db_id()] = t;
    }

    return modes;
}

RoutingData::TransportModes load_transport_modes( RecordIterator<TransportMode>& it )
{
    RoutingData::TransportModes modes;

    do {
        Optional<TransportMode> tm = it.value();
        if ( tm ) {
            modes[ tm->db_id() ] = *tm;
        }
    } while ( it.next() );

    return modes;
}

#if 0
void load_metadata( RoutingData& rd, Db::Connection& connection )
{
    {
        // check that the metadata exists
        bool has_metadata = false;
        {
            Db::Result r( connection.exec( "select * from information_schema.tables where table_name='metadata' and table_schema='tempus_networks'" ) );
            has_metadata = r.size() == 1;
        }
        if ( has_metadata ) {
            Db::Result res( connection.exec( "SELECT key, value FROM tempus.metadata" ) );
            for ( size_t i = 0; i < res.size(); i++ ) {
                std::string k, v;
                res[i][0] >> k;
                res[i][1] >> v;
                rd.set_metadata( k, v );
            }
        }
        else
        {
            // for versions of tempus databases before metadata have been introduced
            rd.set_metadata( "srid", "2154" );
        }
    }
}
#endif
}

