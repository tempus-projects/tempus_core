#ifndef TEMPUS_ROUTING_DATA_HH
#define TEMPUS_ROUTING_DATA_HH

#include "variant.hh"
#include "base.hh"
#include "property.hh"
#include "transport_modes.hh"
#include "optional.hh"

namespace Tempus
{

///
/// Generic class used for the storage of every data needed by plugins
/// It is usually composed of raw graph data
/// As well as additional data like index maps (to map node indexes to db id for instance)
class RoutingData
{
public:
    explicit RoutingData( const std::string& n ) : name_(n) {}

    virtual ~RoutingData() {}

    ///
    /// Name of the data
    std::string name() const { return name_; }

    typedef std::map<db_id_t, TransportMode> TransportModes;
    ///
    /// Access to transport modes
    const TransportModes& transport_modes() const { return transport_modes_; }

    /// 
    /// Transport mode setter
    void set_transport_modes( const TransportModes& );

    ///
    /// access to a transportmode, given its id
    Optional<TransportMode> transport_mode( db_id_t id ) const;

    ///
    /// access to a transportmode, given its name
    Optional<TransportMode> transport_mode( const std::string& name ) const;

private:
    std::string name_;

protected:
    TransportModes transport_modes_ = TransportModes();

private:
    typedef std::map<std::string, Tempus::db_id_t> NameToId;
    ///
    /// Associative array that maps a transport type name to a transport type id
    NameToId transport_mode_from_name_ = NameToId();
};

///
/// Load routing data given its name and options
/// The name is the name of the data builder
/// Routing data are stored globally
std::unique_ptr<RoutingData> load_routing_data( const std::string& data_name, const VariantMap& options = VariantMap() );

void dump_routing_data( const RoutingData* rd, const std::string& file_name );

}

#endif
