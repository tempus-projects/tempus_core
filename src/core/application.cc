/**
 *   Copyright (C) 2012-2013 IFSTTAR (http://www.ifsttar.fr)
 *   Copyright (C) 2012-2013 Oslandia <infos@oslandia.com>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <fstream>

#include "application.hh"
#include "common.hh"
#include "config.hh"

#ifdef _WIN32
#  define NOMINMAX
#  include <windows.h>
#endif

namespace Tempus {

Application::Application()
{
}

Application::~Application()
{
}

Application* Application::instance()
{
    static Application* instance_ = 0;

    if ( 0 == instance_ ) {
        instance_ = new Application();
    }

    return instance_;
}

void Application::set_option( const std::string& key, const Variant& value )
{
    options_[key] = value;
}

Variant Application::option( const std::string& key ) const
{
    auto it = options_.find( key );
    if ( it == options_.end() ) {
        return Variant(std::string(""));
    }
    return it->second;
}

void Application::set_data_directory( const std::string& d )
{
    set_option( "data_directory", Variant::from_string(d) );
}

const std::string Application::data_directory() const
{
    std::string data_dir;
    auto it = options_.find( "data_directory" );
    if ( it != options_.end() ) {
        data_dir = it->second.str();
    }
    else {
        const char* d = getenv( "TEMPUS_DATA_DIRECTORY" );

        if ( !d ) {
            const std::string msg = "environment variable TEMPUS_DATA_DIRECTORY is not defined";
            CERR << msg << "\n";
            throw std::runtime_error( msg );
        }
        data_dir = d;
    }

    // remove trailing space in path (windows will do that for you, an the env var
    // defined in visual studio has that space which screws up concatenation)
    data_dir.erase( data_dir.find_last_not_of( " " )+1 );
    return data_dir;
}


}
