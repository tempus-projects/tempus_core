/**
 *   Copyright (C) 2012-2019 Oslandia <infos@oslandia.com>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */
#ifndef TEMPUS_GRAPH_BUILDER_HH
#define TEMPUS_GRAPH_BUILDER_HH

#include "routing_data_builder.hh"
#include "graph.hh"
#include "db.hh"

namespace Tempus
{

struct ArcRecord
{
    db_id_t node_from_id;
    db_id_t node_to_id;
    Arc arc;
};

class GraphBuilder : public RoutingDataBuilder
{
public:
    GraphBuilder() : RoutingDataBuilder( "multimodal_graph" ) {}

    virtual std::unique_ptr<RoutingData> pg_import( const std::string& pg_options, const VariantMap& options = VariantMap() ) const override;

    //virtual std::unique_ptr<RoutingData> file_import( const std::string& filename, ProgressionCallback& progression, const VariantMap& options = VariantMap() ) const override;
    //virtual void file_export( const RoutingData* rd, const std::string& filename, ProgressionCallback& progression, const VariantMap& options = VariantMap() ) const override;

    static std::unique_ptr<Graph> build_from_iterators( RecordIterator<TransportMode>& tm_iterator,
                                                        RecordIterator<Node>& node_iterator,
                                                        RecordIterator<ArcRecord>& arc_iterator );

    uint32_t version() const override { return 1; }
};

}

#endif
