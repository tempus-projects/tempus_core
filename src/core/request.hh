/**
 *   Copyright (C) 2012-2013 IFSTTAR (http://www.ifsttar.fr)
 *   Copyright (C) 2012-2013 Oslandia <infos@oslandia.com>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TEMPUS_REQUEST_HH
#define TEMPUS_REQUEST_HH

#include "common.hh"
#include "cost.hh"
#include "optional.hh"

#include <list>

namespace Tempus {
/**
   A Request is used to model user requests to the planning engine.

   It is composed of at least an origin and a destination with optional time constraints.
*/

    class Request
    {
    public:

        enum TimeConstraintType {
           NoConstraint = 0,
           /// Arrive before = "many to one" request
           ConstraintBefore,
           /// Depart after = "one to many" request
           ConstraintAfter
        };

        ///
        /// The origin vetex id of the request
        DECLARE_RW_PROPERTY( starting_node, db_id_t );

        ///
        /// Destinations of the request
        DECLARE_RO_PROPERTY( ending_nodes, std::vector<db_id_t> );

        /// 
        /// Initial mode (its ID), used to leave origin (for leave after... constraint)
        DECLARE_RW_PROPERTY( starting_transport_mode, db_id_t );
        
        ///
        /// Final mode (its ID), used to arrive at destination (for arrive before... constraint)
        DECLARE_RW_PROPERTY( ending_transport_mode, db_id_t );
        
        ///
        /// Allowed transport modes (their ID)
        DECLARE_RO_PROPERTY( allowed_transport_modes, std::vector<db_id_t> );

        ///
        /// In PT vehicle time weight
        DECLARE_RW_PROPERTY( pt_time_weight, float );
            
        ///
        /// Waiting time weight    
        DECLARE_RW_PROPERTY( waiting_time_weight, float );
        
        ///
        /// In individual modes time weight
        DECLARE_RW_PROPERTY( indiv_mode_time_weight, float );
        
        ///
        /// Fare (PT or toll) weight
        DECLARE_RW_PROPERTY( fare_weight, float );
        
        ///
        /// PT transfer weight
        DECLARE_RW_PROPERTY( pt_transfer_weight, float );
        
        ///
        /// Mode change weight
        DECLARE_RW_PROPERTY( mode_change_weight, float );
        
        ///
        /// Max walking time, in sec
        DECLARE_RW_PROPERTY( max_walking_time, Optional<int32_t> );
        
        ///
        /// Max time riding a bicycle, in sec
        DECLARE_RW_PROPERTY( max_bicycle_time, Optional<int32_t> );
        
        ///
        /// Max number of PT transfers
        DECLARE_RW_PROPERTY( max_pt_transfers, Optional<int16_t> );
        
        ///
        /// Max number of mode changes
        DECLARE_RW_PROPERTY( max_mode_changes, Optional<int16_t> );

        ///
        /// Type of time constraint of the request.
        DECLARE_RW_PROPERTY_DEFAULT( time_constraint_type, TimeConstraintType, NoConstraint );

        ///
        /// Time constraint.
        DECLARE_RW_PROPERTY( time_constraint, DateTime );

        ///
        /// Isovalue / Isochrone maximum cost value.
        /// If set, destinations are ignored.
        DECLARE_RW_PROPERTY( maximum_cost, Optional<float> );
        
        
        
        /// Construct a request with default values
        Request() = default;

        ///
        /// Construct a request with an origin and a single destination
        /// @param origin The origin id
        /// @param destination A destination id
        Request( db_id_t start_id, db_id_t end_id );
        
        ///
        /// Adds a destination to the request
        void add_ending_node( db_id_t end_node );

        ///
        /// Adds an allowed mode
        void add_allowed_transport_mode( db_id_t transport_mode_id ); 

    };
} // Tempus namespace

#endif
