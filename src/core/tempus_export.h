
#ifndef TEMPUS_EXPORT_H
#define TEMPUS_EXPORT_H

#ifdef _WIN32
#ifndef TEMPUS_EXPORT
#  ifdef TEMPUS_BUILDING
        /* We are building this library */
#    define TEMPUS_EXPORT __declspec(dllexport)
#  else
        /* We are using this library */
#    define TEMPUS_EXPORT __declspec(dllimport)
#  endif
#endif
#else
	#define TEMPUS_EXPORT
#endif

#endif
