create extension if not exists postgis;

SET search_path = tempus, public;

insert into road_node (id, geom) values
(1, 'srid=4326;POINT Z (-1.5554787406003 47.211769838212 0)'::geometry),
(2, 'srid=4326;POINT Z (-1.56431132590517 47.2113982746342 0)'::geometry),
(3, 'srid=4326;POINT Z (-1.57004801980124 47.2110644686148 0)'::geometry),
(4, 'srid=4326;POINT Z (-1.57564576294207 47.2106671849535 0)'::geometry),
(5, 'srid=4326;POINT Z (-1.58346943666639 47.2103276797619 0)'::geometry),
(6, 'srid=4326;POINT Z (-1.59312849285746 47.2099970978537 0)'::geometry),
(7, 'srid=4326;POINT Z (-1.60305699023827 47.2096244432226 0)'::geometry),
(8, 'srid=4326;POINT Z (-1.61295110758723 47.2094515646366 0)'::geometry);

insert into road_section (id, road_type, node_from, node_to, traffic_rules_ft, traffic_rules_tf, length, car_speed_limit, road_name, geom)
select
  n
  , 5
  , n
  , n+1
  , 255
  , 255
  , length
  , 90
  , road_name
  , st_makeline((select geom from road_node where id=n), (select geom from road_node where id=n+1))
from
(
  values
  (1, 600, 'E1'),
  (2, 10000, 'E2'),
  (3, 10000, 'E3'),
  (4, 180, 'E4'),
  (5, 180, 'E5'),
  (6, 10000, 'E6'),
  (7, 180, 'E7')
) t(n, length, road_name);

insert into road_daily_profile (profile_id, begin_time, speed_rule, end_time, average_speed)
values
(1, 0, 5, 1440, 30),
(2,0,5,480,90),
(2,480,5,600,70),
(2,600,5,720,90),
(2,720,5,840,50),
(2,840,5,990,90),
(2,990,5,1200,70),
(2,1200,5,1440,90)
;

insert into road_section_speed (road_section_id, period_id, profile_id)
values
(1,0,1),
(2,0,1),
(4,0,2),
(5,0,2)
;

