/**
 *   Copyright (C) 2016 Oslandia <infos@oslandia.com>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

#include <boost/test/unit_test.hpp>

#include "speed_profiles.hh"

using namespace boost::unit_test ;
using namespace Tempus;

BOOST_AUTO_TEST_SUITE( speed_profiles )

bool floats_equal( float a, float b )
{
    return (a-b)*(a-b) < 0.00001;
}

BOOST_AUTO_TEST_CASE( test_speed_profiles )
{
    {
        RoadEdgeSpeedProfiles sp;
        // 70km/h from 0 to 8am
        // 50km/h from 8am to 9am
        // 30km/h from 9am to 10am
        // 70km/h after
        sp.add_period( 0, SpeedRuleCar, 0, 8*60, 70 );
        sp.add_period( 0, SpeedRuleCar, 8*60, 60, 50 );
        sp.add_period( 0, SpeedRuleCar, 9*60, 60, 30 );
        sp.add_period( 0, SpeedRuleCar, 10*60, 14*60, 70 );

        BOOST_CHECK( floats_equal( sp.time_spent_on_section_forward( 0, SpeedRuleCar, 8*60+30, 70000 ), 102.857 ) );
        BOOST_CHECK( floats_equal( sp.time_spent_on_section_forward( 0, SpeedRuleCar, 8*60, 70000 ), 100.0 ) );
    }

    {
        RoadEdgeSpeedProfiles sp;
        // 50km/h always
        sp.add_period( 0, SpeedRuleCar, 0, 24*60, 50 );

        BOOST_CHECK( floats_equal( sp.time_spent_on_section_forward( 0, SpeedRuleCar, 8*60+30, 70000 ), 84.0 ) );
    }
}

BOOST_AUTO_TEST_CASE( test_speed_profiles_reverse )
{
    {
        RoadEdgeSpeedProfiles sp;
        // 70km/h from 0 to 8am
        // 50km/h from 8am to 9am
        // 30km/h from 9am to 10am
        // 70km/h after
        sp.add_period( 0, SpeedRuleCar, 0, 8*60, 70 );
        sp.add_period( 0, SpeedRuleCar, 8*60, 60, 50 );
        sp.add_period( 0, SpeedRuleCar, 9*60, 60, 30 );
        sp.add_period( 0, SpeedRuleCar, 10*60, 14*60, 70 );
        BOOST_CHECK( floats_equal( sp.time_spent_on_section_backward( 0, SpeedRuleCar, 8*60+20, 70000 ), 65.7143 ) );
    }
    {
        RoadEdgeSpeedProfiles sp;
        // 50km/h always
        sp.add_period( 0, SpeedRuleCar, 0, 24*60, 50 );

        BOOST_CHECK( floats_equal( sp.time_spent_on_section_backward( 0, SpeedRuleCar, 8*60+30, 70000 ), 84.0 ) );
    }
}

BOOST_AUTO_TEST_SUITE_END()

