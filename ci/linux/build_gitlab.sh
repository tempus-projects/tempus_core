#!/bin/sh
mkdir build
cd build
cmake \
  -DCMAKE_BUILD_TYPE=RelWithDebInfo \
  -DBUILD_DOC=OFF \
  ..
make

